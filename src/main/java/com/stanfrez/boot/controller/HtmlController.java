/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stanfrez.boot.controller;

import com.stanfrez.boot.entity.MailData;
import com.stanfrez.boot.service.SendMailService;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.mail.MessagingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author sitkov
 */
@Controller
public class HtmlController {
    
    @Autowired
    SendMailService mailService;

    @PostConstruct
    public void init() {
        System.err.println("INIT CONTROLLERS");
    }

    @RequestMapping("/")
    public String def() {
        return "index";
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/send")
    public void upload(@RequestParam("file") MultipartFile[] files, @RequestParam("name") String name, @RequestParam("phone") String phone, @RequestParam("email") String email, @RequestParam("description") String description) throws IOException {

        byte[] bytes;

        if (files.length != 0) {
            System.out.println(files.length);

            //store file in storage
        }
        System.out.println(name);
        System.out.println(phone);
        System.out.println(email);
        System.out.println(description);
        
        try {
            mailService.sendMessage(files, name, phone, email, description);
        } catch (MessagingException ex) {
            Logger.getLogger(HtmlController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
