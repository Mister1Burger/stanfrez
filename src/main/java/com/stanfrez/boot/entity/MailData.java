/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stanfrez.boot.entity;

import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author sitkov
 */
public class MailData {

    String name;
    String email;
    String phone;
    String description;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "MailData{" + "name=" + name + ", email=" + email + ", phone=" + phone + ", description=" + description + '}';
    }
    
    
}
