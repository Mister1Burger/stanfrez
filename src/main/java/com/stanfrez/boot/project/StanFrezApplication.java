package com.stanfrez.boot.project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@ComponentScan("com.stanfrez.boot")
public class StanFrezApplication {

	public static void main(String[] args) {
		SpringApplication.run(StanFrezApplication.class, args);
	}

}

