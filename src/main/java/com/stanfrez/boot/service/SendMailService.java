
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stanfrez.boot.service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author sitkov
 */
@Service
public class SendMailService {

    private final String MAIL_TO = "burger1488@gmail.com";
    private final String MAIL_SUBJECT = "Потенциальный заказ";

    @Autowired
    public JavaMailSender emailSender;

    public void sendMessage(
            MultipartFile[] files, String name, String phone, String feedbackMail, String description) throws MessagingException {
        // ...
        try {
            MimeMessage message = emailSender.createMimeMessage();

            MimeMessageHelper helper = new MimeMessageHelper(message, true);

            helper.setTo(MAIL_TO);
            helper.setSubject(MAIL_SUBJECT);
            helper.setText("Здравствуйте!\n\n" + "Меня зовут " + name + ".\n"
                    + "Мне нужно изготовить деталь.\n" + "Описание:\n"
                    + description + "\n\n"
                    + "Контакты:\n" + phone + ",\n" + feedbackMail);

            if (files.length > 0) {
                for (MultipartFile file : files) {
                    helper.addAttachment(file.getOriginalFilename(), file);
                }
            }

            emailSender.send(message);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        // ...
    }

}
