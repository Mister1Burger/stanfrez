

var mainApp = angular.module('mainApp', ['ngFileUpload']);








mainApp.controller('IndexController', ['$scope', 'Upload', '$timeout', function ($scope, Upload, $timeout) {
    $(document).ready(function () {

        var menu = $('.menu');
        var origOffsetY = menu.offset().top;

        function scroll() {
            if ($(window).scrollTop() >= origOffsetY) {
                $('.menu').addClass('sticky');

            } else {
                $('.menu').removeClass('sticky');

            }


        }

        document.onscroll = scroll;

    });

    $("ul").find("a").click(function(e) {
        e.preventDefault();
        var section = $(this).attr("href");
        $("html, body").animate({
            scrollTop: $(section).offset().top
        }, 1000);
    });
        $scope.hasNoFiles = true;
        $scope.filesNames = [];
        $scope.loading = false;
        $scope.feedbackUser = {
            name: "",
            email: "",
            phone: "",
            description: "",
            files: []

        };
        var fileupload = $("#attachment");
        fileupload.on('change', function () {


        });
        $scope.fileNameChanged = function (ele) {
            if ($scope.filesNames.length > 0) {
                $scope.filesNames = [];
            }
            var files = ele.files;
            $scope.feedbackUser.files = files;
            console.log("files:" + $scope.feedbackUser.files[0]);
            var l = files.length;

            if (l > 0) {
                $scope.hasNoFiles = false;
                console.log($scope.filesNames);
                for (var i = 0; i < l; i++) {
                    $scope.filesNames[i] = files[i].name;

                    console.log($scope.filesNames);
                }
                $scope.$apply();
            } else {
                $scope.hasNoFiles = false;
            }


        };

        $scope.removeFile = function (index) {
            console.log(index);

            if (index > -1) {
                $scope.feedbackUser.files.splice(index, 1);
            }
            if ($scope.feedbackUser.files.length == 0) {
                $scope.hasNoFiles = true;
                //$scope.filesName[ele] = null;
            }
        };

        $scope.uploadFiles = function (files, errFiles) {
            $scope.feedbackUser.files = files;
            $scope.errFiles = errFiles;
            if (files.length > 0) {
                $scope.hasNoFiles = false;
            }
        };

        $scope.submitForm = function (event) {
            event.preventDefault();
            $scope.loading = true;
             if ($scope.feedbackUser.files.length != 0) {
                 console.log("files")
                if (checkSize($scope.feedbackUser.files)) {
                    Upload.upload({
                        url: 'send',
                        data:{
                        file: $scope.feedbackUser.files,
                        'name': $scope.feedbackUser.name,
                            'phone': $scope.feedbackUser.phone,
                            'email': $scope.feedbackUser.email,
                            'description': $scope.feedbackUser.description
                    },
                    arrayKey: ''

                    }).then(function (response) {
                $timeout(function () {
                    if(response.status >= 200){
                        $scope.feedbackUser = {
                            name: "",
                            email: "",
                            phone: "",
                            description: "",
                            files: []

                        };
                         $scope.progress = -1;
                         $scope.hasNoFiles = true;
                         $scope.loading = false;
                        alert("Успешно");
                    }
                });
            }, function (response) {
                if (response.status >= 400) {
                    $scope.loading = false;
                    $scope.errorMsg = response.status + ': ' + response.data;
                    $scope.progress = -1;
                }
            });
//    .then(function (response) {
//                        $timeout(function () {
//                            $scope.result = response.data;
//                        });
//                    }, function (response) {
//                        if (response.status > 0) {
//                            $scope.errorMsg = response.status + ': ' + response.data;
//                        }
//                    }, function (evt) {
//                        $scope.progress =
//                                Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
//                    }).success(function (data, status, headers, config) {
//                       
//                    });
                } else {
                    $scope.loading = false;
                    alert("Файлы до 50МБ!");
                }
            } else if ($scope.feedbackUser.description != "") {
                console.log("description");
                $scope.loading = true;
                 Upload.upload({
                        url: 'send',
                        data:{
                        file: $scope.feedbackUser.files,
                        'name': $scope.feedbackUser.name,
                            'phone': $scope.feedbackUser.phone,
                            'email': $scope.feedbackUser.email,
                            'description': $scope.feedbackUser.description
                    },
                    arrayKey: ''

                    }).then(function (response) {
                $timeout(function () {
                    if(response.status >= 200){
                        $scope.feedbackUser = {
                            name: "",
                            email: "",
                            phone: "",
                            description: "",
                            files: []

                        };
                         $scope.progress = -1;
                         $scope.hasNoFiles = true;
                         $scope.loading = false;
                        alert("Успешно");
                    }
                });
            }, function (response) {
                if (response.status >= 400) {
                    $scope.loading = false;
                    $scope.errorMsg = response.status + ': ' + response.data;
                    $scope.progress = -1;
                }
            });
            } else {
                $scope.loading = false;
                alert("Прикрепите файлы или напишите описание");
            }

        };

        checkSize = function (files) {
            var size = 0;
            angular.forEach($scope.files, function (file) {
                size = size + file.size;
            });
            console.log(size);
            //TODO add 1 more "0" for 50000000
            if (size > 5000000) {
                return false;
            } else {
                return true;
            }

        };

        $scope.slickRulesConfig = {
            slidesToShow: 4,

            autoplay: true, autoplaySpeed: 12000,
            responsive: [
                {
                    breakpoint: 1000,
                    settings: {
                        slidesToShow: 3
                    }
                },
                {
                    breakpoint: 820,
                    settings: {
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ],
            method: {}
        };

    }]);

